from setuptools import setup

setup(
    name='pyserial-trt',
    # version='0.0.1',
    # description='pyserial reader thread with timeout event',
    packages=['serialtrt'],
    install_requires=['pyserial']
)
